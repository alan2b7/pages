<!DOCTYPE html>
<html lang="en">
<head>
<!-- lun. 10 mars 2025 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>AppArmor</title>
<meta name="author" content="alan" />
<meta name="generator" content="Org Mode" />
<link rel="icon" type="image/png" href="/img/favicon.png">
<link rel="stylesheet" type="text/css" href="/css/style.css"/>
</head>
<body>
<header id="preamble" class="status">
<nav class="nav">
<a class="nav-link" href="/index.html">Home</a>
<a class="nav-link" href="/wiki/index.html">Wiki</a>
<a class="nav-link" href="https://userstyles.world/user/alan" rel="noreferrer">Userstyles</a>
<a class="nav-link" href="https://codeberg.org/alan2b7" rel="noreferrer">Codeberg</a></nav>
</header>
<main id="content" class="content">
<h1 class="title">AppArmor</h1>
<div id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#org6854ffc">1. Description</a></li>
<li><a href="#org1decc4d">2. Enable AppArmor</a></li>
<li><a href="#org36d520d">3. Profiles</a></li>
<li><a href="#org65ae879">4. Status</a></li>
<li><a href="#org2357af1">5. Boot time</a></li>
<li><a href="#org9cc2fe7">6. References</a></li>
</ul>
</div>
</div>

<div id="outline-container-org6854ffc" class="outline-2">
<h2 id="org6854ffc"><span class="section-number-2">1.</span> Description</h2>
<div class="outline-text-2" id="text-1">
<ul class="org-ul">
<li><a href="https://debian-handbook.info/browse/stable/sect.apparmor.html#sect.apparmor-principles">https://debian-handbook.info/browse/stable/sect.apparmor.html#sect.apparmor-principles</a></li>
</ul>

<blockquote>
<p>
AppArmor is a <a href="https://en.wikipedia.org/wiki/Mandatory_access_control">Mandatory Access Control (MAC)</a> system built on Linux&rsquo;s <a href="https://en.wikipedia.org/wiki/Linux_Security_Modules">LSM (Linux Security Modules)</a> interface. In practice, the kernel queries AppArmor before each system call to know whether the process is authorized to do the given operation. Through this mechanism, AppArmor confines programs to a limited set of resources.
</p>

<p>
AppArmor applies a set of rules (known as “profile”) on each program. The profile applied by the kernel depends on the installation path of the program being executed. Contrary to SELinux (discussed in  <a href="https://debian-handbook.info/browse/stable/sect.selinux.html">Section 14.5, “Introduction to SELinux”</a>), the rules applied do not depend on the user. All users face the same set of rules when they are executing the same program (but traditional user permissions still apply and might result in different behavior!).
</p>

<p>
AppArmor profiles are stored in <code>/etc/apparmor.d/</code> and they contain a list of access control rules on resources that each program can make use of. The profiles are compiled and loaded into the kernel by the <code>apparmor_parser</code> command. Each profile can be loaded either in enforcing or complaining mode. The former enforces the policy and reports violation attempts, while the latter does not enforce the policy but still logs the system calls that would have been denied.
</p>
</blockquote>
</div>
</div>

<div id="outline-container-org1decc4d" class="outline-2">
<h2 id="org1decc4d"><span class="section-number-2">2.</span> Enable AppArmor</h2>
<div class="outline-text-2" id="text-2">
<p>
To load all AppArmor profiles on startup, enable its <a href="systemd.html">systemd</a> service (enabled by default on <a href="debian.html">Debian</a>)
</p>

<div class="org-src-container">
<pre class="src src-sh">systemctl enable apparmor
</pre>
</div>

<p>
Find out if AppArmor is enabled (returns <code>Y</code> if true) or with the <a href="https://manpages.debian.org/stable/apparmor/aa-enabled.1">aa-enabled(1)</a> command
</p>

<div class="org-src-container">
<pre class="src src-sh">cat /sys/module/apparmor/parameters/enabled
</pre>
</div>

<p>
Check the current state of AppArmor with <a href="https://manpages.debian.org/stable/apparmor/aa-status.8">aa-status(8)</a>
</p>

<pre class="example" id="org6f8ecbc">
# aa-status
apparmor module is loaded.
18 profiles are loaded.
18 profiles are in enforce mode.
 ...
0 profiles are in complain mode.
0 processes have profiles defined.
0 processes are in enforce mode.
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.
</pre>
</div>
</div>

<div id="outline-container-org36d520d" class="outline-2">
<h2 id="org36d520d"><span class="section-number-2">3.</span> Profiles</h2>
<div class="outline-text-2" id="text-3">
<p>
Profiles are stored under <code>/etc/apparmor.d</code>, on <a href="debian.html">Debian</a> install <a href="https://packages.debian.org/stable/apparmor-profiles">apparmor-profiles</a> package to add profiles managed by the AppArmor community and <a href="https://packages.debian.org/stable/apparmor-profiles-extra">apparmor-profiles-extra</a> for profiles developed by Debian and Ubuntu.
</p>

<p>
Install <a href="https://packages.debian.org/stable/apparmor-utils">apparmor-utils</a> package on <a href="debian.html">Debian</a> to manage profiles
</p>

<p>
Links for creating new profiles:
</p>
<ul class="org-ul">
<li><a href="https://gitlab.com/apparmor/apparmor/-/wikis/QuickProfileLanguage">https://gitlab.com/apparmor/apparmor/-/wikis/QuickProfileLanguage</a></li>
<li><a href="https://debian-handbook.info/browse/stable/sect.apparmor.html#sect.apparmor-new-profile">https://debian-handbook.info/browse/stable/sect.apparmor.html#sect.apparmor-new-profile</a></li>
<li><a href="https://wiki.archlinux.org/title/AppArmor#Auditing_and_generating_profiles">https://wiki.archlinux.org/title/AppArmor#Auditing_and_generating_profiles</a></li>
</ul>
</div>
</div>

<div id="outline-container-org65ae879" class="outline-2">
<h2 id="org65ae879"><span class="section-number-2">4.</span> Status</h2>
<div class="outline-text-2" id="text-4">
<p>
Use <a href="https://manpages.debian.org/stable/apparmor/aa-status.8">aa-status(8)</a> to list all loaded profiles for applications/processes and their status (enforced, complain, unconfined)
</p>

<div class="org-src-container">
<pre class="src src-sh">aa-status
</pre>
</div>

<p>
List running executables that are confined by a profile
</p>

<div class="org-src-container">
<pre class="src src-sh">ps auxZ | grep -v <span class="org-string">'^unconfined'</span>
</pre>
</div>

<p>
Use <a href="https://manpages.debian.org/stable/apparmor-utils/aa-unconfined.8">aa-unconfined(8)</a> to list processes with tcp/udp ports that are not confined
</p>

<div class="org-src-container">
<pre class="src src-sh">aa-unconfined
</pre>
</div>
</div>
</div>

<div id="outline-container-org2357af1" class="outline-2">
<h2 id="org2357af1"><span class="section-number-2">5.</span> Boot time</h2>
<div class="outline-text-2" id="text-5">
<ul class="org-ul">
<li><a href="https://wiki.archlinux.org/title/AppArmor#Speed-up_AppArmor_start_by_caching_profiles">https://wiki.archlinux.org/title/AppArmor#Speed-up_AppArmor_start_by_caching_profiles</a></li>
</ul>

<blockquote>
<p>
Since AppArmor has to translate the configured profiles into a binary format it may significantly increase the boot time. You can check current AppArmor startup time with <a href="https://manpages.debian.org/stable/systemd/systemd-analyze.1">systemd-analyze(1)</a>
</p>
</blockquote>

<div class="org-src-container">
<pre class="src src-sh">systemd-analyze blame | grep apparmor
</pre>
</div>
</div>
</div>

<div id="outline-container-org9cc2fe7" class="outline-2">
<h2 id="org9cc2fe7"><span class="section-number-2">6.</span> References</h2>
<div class="outline-text-2" id="text-6">
<ul class="org-ul">
<li><a href="https://apparmor.net/">https://apparmor.net/</a></li>
<li><a href="https://debian-handbook.info/browse/stable/sect.apparmor.html">https://debian-handbook.info/browse/stable/sect.apparmor.html</a></li>
<li><a href="https://gitlab.com/apparmor/apparmor/-/wikis/home">https://gitlab.com/apparmor/apparmor/-/wikis/home</a></li>
<li><a href="https://wiki.archlinux.org/title/AppArmor">https://wiki.archlinux.org/title/AppArmor</a></li>
<li><a href="https://wiki.debian.org/AppArmor">https://wiki.debian.org/AppArmor</a></li>
</ul>
</div>
</div>
</main>
<footer id="postamble" class="status">
<div class="status"><p class="updated">sam. 15 février 2025</p>
<p class="creator">Generated with <a href="https://www.gnu.org/software/emacs/">Emacs</a> 28.2 (<a href="https://orgmode.org">Org</a> mode 9.5.5)</p></div>
</footer>
</body>
</html>
